import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ConnexionComponent } from './pages/connexion/connexion.component';
import { InscriptionComponent } from './pages/inscription/inscription.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { ActionsComponent } from './pages/actions/actions.component'; // Importez FormsModule
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MenuComponent } from './components/menu/menu.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { CreateprestataireComponent } from './pages/createprestataire/createprestataire.component';
import { GererprestataireComponent } from './pages/gererprestataire/gererprestataire.component';
import { GererusersComponent } from './pages/gererusers/gererusers.component';
import { ParametrerComponent } from './pages/parametrer/parametrer.component';
import { RapportsComponent } from './pages/rapports/rapports.component';
import { MatCardModule } from '@angular/material/card';
import { AuthService } from './services/authentification.service';
import { AuthGuard } from './guards/guards/auth.guard';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ModifierPrestataireDialogComponent } from './components/modifier-prestataire-dialog/modifier-prestataire-dialog.component';
import { MatSliderModule } from '@angular/material/slider';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AccueilComponent,
    ConnexionComponent,
    InscriptionComponent,
    ActionsComponent,
    MenuComponent,
    CreateprestataireComponent,
    GererprestataireComponent,
    GererusersComponent,
    ParametrerComponent,
    RapportsComponent,
    ModifierPrestataireDialogComponent
  ],
  imports: [
    BrowserModule,
    MatSliderModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatIconModule,
    MatGridListModule ,
    MatTableModule ,
    MatPaginatorModule,
    MatSidenavModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDialogModule,
    MatMenuModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    MatListModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyBKTfv6m08_1RLVEiRxjO24kgepFSJtl5c",
  authDomain: "ligeylu-44b3d.firebaseapp.com",
  projectId: "ligeylu-44b3d",
  storageBucket: "ligeylu-44b3d.appspot.com",
  messagingSenderId: "459631615611",
  appId: "1:459631615611:web:5f2ba9187ba5925fd00cde",
  measurementId: "G-9DTP19ZB78"
    }),
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
