import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-modifier-prestataire-dialog',
  templateUrl: './modifier-prestataire-dialog.component.html',
  styleUrls: ['./modifier-prestataire-dialog.component.css']
})
export class ModifierPrestataireDialogComponent {
  editForm: FormGroup;
  regions: string[] = ['Région 1', 'Région 2', 'Région 3']; // Exemple de régions

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ModifierPrestataireDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private afs: AngularFirestore
  ) {
    this.editForm = this.fb.group({
      name: [data.name || '', Validators.required],
      surname: [data.surname || '', Validators.required],
      email: [data.email || '', [Validators.required, Validators.email]],
      phone: [data.phone || '', Validators.required],
      region: [data.region || '', Validators.required],
      description: [data.description || '', Validators.required],
      function: [data.function || '', Validators.required],
      tarification: [data.tarification || '', Validators.required],
      distance: [data.distance || '', Validators.required],
    });
  }

  onSubmit(): void {
    const updatedData = this.editForm.value;

    const nonEmptyData = Object.keys(updatedData)
      .filter(key => updatedData[key] !== '' && updatedData[key] !== null && updatedData[key] !== undefined)
      .reduce((obj: { [key: string]: any }, key: string) => {
        obj[key] = updatedData[key];
        return obj;
      }, {});

    if (this.data.id) {
      this.afs.collection('prestataire').doc(this.data.id).get().subscribe((doc) => {
        if (doc.exists) {
          this.afs.collection('prestataire').doc(this.data.id).update(nonEmptyData)
            .then(() => {
              console.log('Données mises à jour avec succès');
              this.dialogRef.close(true);
            })
            .catch((error) => {
              console.error('Erreur lors de la mise à jour des données :', error);
            });
        } else {
          console.error('Le document n\'existe pas :', this.data.id);
        }
      });
    } else {
      console.error('ID du document non fourni');
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
