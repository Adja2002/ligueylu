import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierPrestataireDialogComponent } from './modifier-prestataire-dialog.component';

describe('ModifierPrestataireDialogComponent', () => {
  let component: ModifierPrestataireDialogComponent;
  let fixture: ComponentFixture<ModifierPrestataireDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModifierPrestataireDialogComponent]
    });
    fixture = TestBed.createComponent(ModifierPrestataireDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
