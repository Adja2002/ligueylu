import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private auth: AngularFireAuth, private firestore: AngularFirestore) {}

  async register(email: string, password: string, nom: string, prenom: string, role: string, telephone: string) {
    try {
      const credential = await this.auth.createUserWithEmailAndPassword(email, password);
      const user = credential.user;
      if (user) {
        await this.firestore.collection('users').doc(user.uid).set({
          email,
          nom,
          prenom,
          role,
          telephone
        });
        return user;
      }
      return null;
    } catch (error) {
      console.error('Error registering user:', error);
      return null;
    }
  }

  async login(email: string, password: string) {
    try {
      const userCredential = await this.auth.signInWithEmailAndPassword(email, password);
      return userCredential.user;
    } catch (error) {
      console.error('Error logging in:', error);
      throw error;
    }
  }

  getUser(): Observable<any> {
    return this.auth.authState.pipe(
      map(user => {
        if (user) {
          return this.firestore.collection('users').doc(user.uid).valueChanges();
        } else {
          return null;
        }
      })
    );
  }

  isLoggedIn(): Observable<boolean> {
    return this.auth.authState.pipe(
      map(user => {
        console.log('Auth state user:', user);
        return !!user;
      })
    );
  }

  async logout() {
    try {
      await this.auth.signOut();
      console.log('User logged out successfully');
    } catch (error) {
      console.error('Error logging out:', error);
    }
  }

  // Nouvelle méthode pour récupérer tous les utilisateurs
  getAllUsers(): Observable<any[]> {
    return this.firestore.collection('users').valueChanges();
  }

  async deleteUser(userId: string) {
    try {
      await this.firestore.collection('users').doc(userId).delete();
      console.log('Utilisateur supprimé avec succès.');
    } catch (error) {
      console.error('Erreur lors de la suppression de l\'utilisateur:', error);
    }
  }
}
