import { Component } from '@angular/core';
import { AuthService } from './services/authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ligueylu';
  constructor(private authService: AuthService) {
    this.authService.logout();
  }

}
