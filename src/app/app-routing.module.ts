import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ConnexionComponent } from './pages/connexion/connexion.component';
import { InscriptionComponent } from './pages/inscription/inscription.component';
import { ActionsComponent } from './pages/actions/actions.component';
import { AuthGuard } from './guards/guards/auth.guard';
import { GererprestataireComponent } from './pages/gererprestataire/gererprestataire.component';
import { CreateprestataireComponent } from './pages/createprestataire/createprestataire.component';
import { GererusersComponent } from './pages/gererusers/gererusers.component';
import { ParametrerComponent } from './pages/parametrer/parametrer.component';
import { RapportsComponent } from './pages/rapports/rapports.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'accueil', component: AccueilComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'create-prestataire', component: CreateprestataireComponent,canActivate: [AuthGuard] },
  { path: 'manage-prestataire', component: GererprestataireComponent ,canActivate: [AuthGuard]},
  { path: 'manage-users', component: GererusersComponent,canActivate: [AuthGuard] },
  { path: 'parametrer', component: ParametrerComponent ,canActivate: [AuthGuard]},
  { path: 'rapports', component: RapportsComponent,canActivate: [AuthGuard] },
  { path: 'actions', component: ActionsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
