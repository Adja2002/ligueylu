import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from 'src/app/services/authentification.service';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.authService.isLoggedIn().pipe(
      take(1),
      map(isLoggedIn => {
        console.log('isLoggedIn:', isLoggedIn); // Ajoutez ce journal
        if (!isLoggedIn) {
          console.log('User is not logged in, redirecting to /connexion');
          this.router.navigate(['/connexion']);
          return false;
        }
        console.log('User is logged in, access granted');
        return true;
      })
    );
  }
}
