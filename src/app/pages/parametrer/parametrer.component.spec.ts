import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrerComponent } from './parametrer.component';

describe('ParametrerComponent', () => {
  let component: ParametrerComponent;
  let fixture: ComponentFixture<ParametrerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ParametrerComponent]
    });
    fixture = TestBed.createComponent(ParametrerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
