import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ModifierPrestataireDialogComponent } from 'src/app/components/modifier-prestataire-dialog/modifier-prestataire-dialog.component';

@Component({
  selector: 'app-gererprestataire',
  templateUrl: './gererprestataire.component.html',
  styleUrls: ['./gererprestataire.component.css']
})
export class GererprestataireComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name','surname' ,'email', 'phone', 'function', 'actions'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  constructor(private afs: AngularFirestore, private dialog: MatDialog) {
    this.dataSource = new MatTableDataSource<any>([]);
  }
  ngOnInit(): void {
    this.fetchPrestataires();
  }
  fetchPrestataires() {
    this.afs.collection('prestataire').snapshotChanges().subscribe((prestataires: any[]) => {
      this.dataSource.data = prestataires.map(prestataire => ({
        id: prestataire.payload.doc.id,
        ...prestataire.payload.doc.data()
      }));
      this.dataSource.paginator = this.paginator;
    });
  }
  editPrestataire(prestataire: any) {
    const dialogRef = this.dialog.open(ModifierPrestataireDialogComponent, {
      width: '600px',
      height:'70vh',
      panelClass: 'custom-dialog',
      data: prestataire
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.fetchPrestataires();
      }
    });
  }

  deletePrestataire(prestataire: any) {
    this.afs.collection('prestataire').doc(prestataire.id).delete().then(() => {
      console.log('Prestataire supprimé');
      this.fetchPrestataires();
    }).catch(error => {
      console.error('Erreur lors de la suppression du prestataire :', error);
    });
  }

  onPageChange(event: PageEvent) {
    console.log(event);
  }
}
