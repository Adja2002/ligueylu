import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GererprestataireComponent } from './gererprestataire.component';

describe('GererprestataireComponent', () => {
  let component: GererprestataireComponent;
  let fixture: ComponentFixture<GererprestataireComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GererprestataireComponent]
    });
    fixture = TestBed.createComponent(GererprestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
