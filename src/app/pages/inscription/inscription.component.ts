import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/authentification.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent {
  nom: string = '';
  prenom: string = '';
  email: string = '';
  mdp: string = '';
  confirmationMdp: string = '';
  role: string = '';
  telephone: string = '';
  inscriptionSuccess: boolean = false;

  constructor(private authService: AuthService) {}

  onSubmit(form: any) {
    if (form.valid) {
      const { email, mdp, nom, prenom, role, telephone } = form.value;

      this.authService.register(email, mdp, nom, prenom, role, telephone)
        .then((user) => {
          console.log('Inscription réussie!', user);
          this.inscriptionSuccess = true;
        })
        .catch((error) => {
          console.error('Erreur lors de l\'inscription:', error);
        });
    }
  }

  closeModal() {
    this.inscriptionSuccess = false;
  }
}
