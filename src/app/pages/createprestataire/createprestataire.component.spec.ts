import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateprestataireComponent } from './createprestataire.component';

describe('CreateprestataireComponent', () => {
  let component: CreateprestataireComponent;
  let fixture: ComponentFixture<CreateprestataireComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateprestataireComponent]
    });
    fixture = TestBed.createComponent(CreateprestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
