import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-createprestataire',
  templateUrl: './createprestataire.component.html',
  styleUrls: ['./createprestataire.component.css']
})
export class CreateprestataireComponent implements OnInit {
  createProviderForm!: FormGroup;
  errorMessage: string = '';
  inscriptionSuccess: boolean = false;
  regions: string[] = [
    'Dakar', 'Diourbel', 'Fatick', 'Kaffrine', 'Kaolack', 'Kédougou', 'Kolda',
    'Louga', 'Matam', 'Saint-Louis', 'Sédhiou', 'Tambacounda', 'Thiès', 'Ziguinchor'
  ];

  constructor(private afs: AngularFirestore, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createProviderForm = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      region: ['', Validators.required],
      description: ['', Validators.required],
      function: ['', Validators.required],
      tarification: ['', Validators.required],
      distance: ['', Validators.required]
    });
  }

  createProvider(): void {
    const data = this.createProviderForm.value;
    this.afs.collection('prestataire').ref.where('email', '==', data.email).get()
      .then(snapshot => {
        if (snapshot.size > 0) {
          this.errorMessage = 'Un prestataire avec cette adresse e-mail existe déjà.';
        } else {
          this.afs.collection('prestataire').ref.where('name', '==', data.name).get()
            .then(snapshot => {
              if (snapshot.size > 0) {
                this.errorMessage = 'Un prestataire avec ce nom existe déjà.';
              } else {
                this.afs.collection('prestataire').add(data)
                  .then(() => {
                    console.log('Données ajoutées avec succès à Firestore');
                    this.inscriptionSuccess = true;
                    this.errorMessage = '';
                  })
                  .catch((error) => {
                    console.error('Erreur lors de l\'ajout des données à Firestore :', error);
                    this.errorMessage = 'Une erreur s\'est produite lors de la sauvegarde du prestataire. Veuillez réessayer plus tard.';
                  });
              }
            });
        }
      });
  }

  closeModal() {
    this.inscriptionSuccess = false;
  }
}
