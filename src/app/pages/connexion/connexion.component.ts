import { Component, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/authentification.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent {
  email: string = '';
  password: string = '';
  errorMessage: string = '';

  @ViewChild('emailInput') emailInput: NgForm | undefined;
  @ViewChild('mdpInput') mdpInput: NgForm | undefined;

  constructor(private authService: AuthService , private router: Router) {}

  async login() {
    try {
      const user = await this.authService.login(this.email, this.password);
      // Gérer la connexion réussie ici, par exemple, rediriger l'utilisateur vers une autre page
      console.log('Connexion réussie !', user);
      this.router.navigate(['/actions']);

    } catch (error) {
      console.error('Error logging in:', error);
      if (error instanceof Error) {
        this.errorMessage = "Erreur lors de la connexion : " + error.message;
      } else {
        this.errorMessage = "Erreur lors de la connexion.";
      }
      // Gérer l'erreur de connexion ici, par exemple, afficher un message à l'utilisateur
    }
  }
}
