import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/authentification.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-gererusers',
  templateUrl: './gererusers.component.html',
  styleUrls: ['./gererusers.component.css']
})
export class GererusersComponent implements OnInit {
  utilisateurs: any[] = []; // Tableau pour stocker les utilisateurs
  displayedColumns: string[] = ['nom', 'prenom', 'email', 'role', 'telephone', 'actions']; // Colonnes à afficher dans le tableau

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.getUsers(); // Chargez les utilisateurs au démarrage du composant
  }

  getUsers() {
    this.authService.getAllUsers()
      .subscribe((users) => {
        this.utilisateurs = users;
      }, (error) => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
  }

  deleteUser(userId: string) {
    this.authService.deleteUser(userId)
      .then(() => {
        console.log('Utilisateur supprimé avec succès.');
        // Rafraîchissez la liste des utilisateurs après la suppression
        this.getUsers();
      })
      .catch((error) => {
        console.error('Erreur lors de la suppression de l\'utilisateur:', error);
      });
  }

  onPageChange(event: PageEvent) {
    // Ici vous pouvez implémenter la logique pour charger les données de la nouvelle page
    console.log('Changement de page:', event);
    // Par exemple, vous pouvez appeler une méthode pour recharger les données
    // this.getUsers();
  }
}
