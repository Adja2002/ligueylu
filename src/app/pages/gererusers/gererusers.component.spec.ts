import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GererusersComponent } from './gererusers.component';

describe('GererusersComponent', () => {
  let component: GererusersComponent;
  let fixture: ComponentFixture<GererusersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GererusersComponent]
    });
    fixture = TestBed.createComponent(GererusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
